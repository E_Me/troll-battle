Troll Battle

This is a short mobile game for Android made with Unity 3D Engine. The player's main goal is to slay the troll that lives inside a castle. The ControlPlayer.cs is a part from the game's script and describes the behavior of the main character that the player controls. 

Some screenshots from the game:

![](Images/Img1.png)

![](Images/Img2.png)

![](Images/Img3.png)
